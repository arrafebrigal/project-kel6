import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel6/config/asset.dart';
import 'package:project_kel6/event/event_db.dart';
import 'package:project_kel6/model/data_pengajuan.dart';
import 'package:project_kel6/screen/admin/add_update_pengajuan.dart';

class ListDataPengajuan extends StatefulWidget {
  @override
  State<ListDataPengajuan> createState() => _ListDataPengajuanState();
}

class _ListDataPengajuanState extends State<ListDataPengajuan> {
  List<DataPengajuan> _listDataPengajuan = [];

  void getDataPengajuan() async {
    _listDataPengajuan = await EventDb.getDataPengajuan();

    setState(() {});
  }

  @override
  void initState() {
    getDataPengajuan();
    super.initState();
  }

  void showOption(DataPengajuan? data_pengajuan) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuan(data_pengajuan: data_pengajuan))
            ?.then((value) => getDataPengajuan());
        break;
      case 'delete':
        EventDb.deleteDataPengajuan(data_pengajuan!.kode_pengajuan!)
            .then((value) => getDataPengajuan());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Data Pengajuan'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listDataPengajuan.length > 0
              ? ListView.builder(
                  itemCount: _listDataPengajuan.length,
                  itemBuilder: (context, index) {
                    DataPengajuan dataPengajuan = _listDataPengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(dataPengajuan.nama_peminjam ?? ''),
                      subtitle: Text(dataPengajuan.npm_peminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(dataPengajuan),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdatePengajuan())
                  ?.then((value) => getDataPengajuan()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}

class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const _defaultHeight = 56.0;

  final double elevation;
  final Gradient gradient;
  final Widget title;
  final double barHeight;

  GradientAppBar(
      {this.elevation = 3.0,
      required this.gradient,
      required this.title,
      this.barHeight = _defaultHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          offset: Offset(0, elevation),
          color: Colors.black.withOpacity(0.3),
          blurRadius: 3,
        ),
      ]),
      child: AppBar(
        title: title,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight);
}
