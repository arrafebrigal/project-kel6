import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:project_kel6/config/asset.dart';
import 'package:project_kel6/event/event_db.dart';
import 'package:project_kel6/screen/admin/list_user.dart';
import 'package:project_kel6/widget/info.dart';

import '../../model/data_pengembalian.dart';

class AddUpdateDataPengembalian extends StatefulWidget {
  final DataPengembalian? data_pengembalian;
  AddUpdateDataPengembalian({this.data_pengembalian});

  @override
  State<AddUpdateDataPengembalian> createState() =>
      _AddUpdateDataPengembalianState();
}

class _AddUpdateDataPengembalianState extends State<AddUpdateDataPengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKodePengembalian = TextEditingController();
  var _controllerKodePengajuan = TextEditingController();
  var _controllerTanggal = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengembalian != null) {
      _controllerKodePengembalian.text =
          widget.data_pengembalian!.kode_pengembalian!;
      _controllerKodePengajuan.text = widget.data_pengembalian!.kode_pengajuan!;
      _controllerTanggal.text = widget.data_pengembalian!.tanggal_kembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Data Pengembalian'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggal,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengembalian == null) {
                        String message = await EventDb.addDataPengembalian(
                          _controllerKodePengembalian.text,
                          _controllerKodePengajuan.text,
                          _controllerTanggal.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKodePengembalian.clear();
                          _controllerKodePengajuan.clear();
                          _controllerTanggal.clear();
                          Get.off(
                            DataPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdateDataPengembalian(
                          _controllerKodePengembalian.text,
                          _controllerKodePengajuan.text,
                          _controllerTanggal.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('_isHidden', _isHidden));
  }
}
