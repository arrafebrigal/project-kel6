import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel6/config/asset.dart';
import 'package:project_kel6/event/event_db.dart';
import 'package:project_kel6/model/data_pengembalian.dart';
import 'package:project_kel6/screen/admin/add_update_pengembalian.dart';

class ListDataPengembalian extends StatefulWidget {
  @override
  State<ListDataPengembalian> createState() => _ListDataPengembalianState();
}

class _ListDataPengembalianState extends State<ListDataPengembalian> {
  List<DataPengembalian> _listDataPengembalian = [];

  void getDataPengembalian() async {
    _listDataPengembalian = await EventDb.getDataPengembalian();
    setState(() {});
  }

  @override
  void initState() {
    getDataPengembalian();
    super.initState();
  }

  void showOption(DataPengembalian? data_pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateDataPengembalian(data_pengembalian: data_pengembalian))
            ?.then((value) => getDataPengembalian());
        break;
      case 'delete':
        EventDb.deleteDataPengembalian(data_pengembalian!.kode_pengembalian!)
            .then((value) => getDataPengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: Text('List Data Pengembalian'),
        // backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listDataPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listDataPengembalian.length,
                  itemBuilder: (context, index) {
                    DataPengembalian dataPengembalian =
                        _listDataPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(dataPengembalian.kode_pengembalian ?? ''),
                      subtitle: Text(dataPengembalian.tanggal_kembali ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(dataPengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdateDataPengembalian())
                  ?.then((value) => getDataPengembalian()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}

class GradientAppBar extends StatelessWidget implements PreferredSizeWidget {
  static const _defaultHeight = 56.0;

  final double elevation;
  final Gradient gradient;
  final Widget title;
  final double barHeight;

  GradientAppBar(
      {this.elevation = 3.0,
      required this.gradient,
      required this.title,
      this.barHeight = _defaultHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      decoration: BoxDecoration(gradient: gradient, boxShadow: [
        BoxShadow(
          offset: Offset(0, elevation),
          color: Colors.black.withOpacity(0.3),
          blurRadius: 3,
        ),
      ]),
      child: AppBar(
        title: title,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(barHeight);
}
