import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:project_kel6/config/asset.dart';
import 'package:project_kel6/event/event_db.dart';
import 'package:project_kel6/screen/admin/list_pengajuan.dart';
import 'package:project_kel6/widget/info.dart';

import '../../model/data_pengajuan.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final DataPengajuan? data_pengajuan;
  AddUpdatePengajuan({this.data_pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode_pengajuan = TextEditingController();
  var _controllertanggal = TextEditingController();
  var _controllernpm_peminjam = TextEditingController();
  var _controllernama_peminjam = TextEditingController();
  var _controllerprodi = TextEditingController();
  var _controllerno_handphone = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengajuan != null) {
      _controllerkode_pengajuan.text = widget.data_pengajuan!.kode_pengajuan!;
      _controllertanggal.text = widget.data_pengajuan!.tanggal!;
      _controllernpm_peminjam.text = widget.data_pengajuan!.npm_peminjam!;
      _controllernama_peminjam.text = widget.data_pengajuan!.nama_peminjam!;
      _controllerprodi.text = widget.data_pengajuan!.prodi!;
      _controllerno_handphone.text = widget.data_pengajuan!.no_handphone!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        // titleSpacing: 0,
        title: widget.data_pengajuan != null
            ? Text('Update Pengajuan')
            : Text('Tambah Pengajuan'),
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_pengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  enabled: widget.data_pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernpm_peminjam,
                  decoration: InputDecoration(
                      labelText: "NPM",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernama_peminjam,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerprodi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerno_handphone,
                  decoration: InputDecoration(
                      labelText: "No Handphone",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengajuan == null) {
                        String message = await EventDb.AddDataPengajuan(
                          _controllerkode_pengajuan.text,
                          _controllertanggal.text,
                          _controllernpm_peminjam.text,
                          _controllernama_peminjam.text,
                          _controllerprodi.text,
                          _controllerno_handphone.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode_pengajuan.clear();
                          _controllertanggal.clear();
                          _controllernpm_peminjam.clear();
                          _controllernama_peminjam.clear();
                          _controllerprodi.clear();
                          _controllerno_handphone.clear();
                          Get.off(
                            ListDataPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdateDataPengajuan(
                          _controllerkode_pengajuan.text,
                          _controllertanggal.text,
                          _controllernpm_peminjam.text,
                          _controllernama_peminjam.text,
                          _controllerprodi.text,
                          _controllerno_handphone.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('_isHidden', _isHidden));
  }
}
