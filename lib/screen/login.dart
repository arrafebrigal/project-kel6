import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:project_kel6/config/asset.dart';
import 'package:project_kel6/event/event_db.dart';
import 'package:project_kel6/screen/admin/list_user.dart';
import 'admin/reset_password.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var _controllerUserName = TextEditingController();
  var _controllerPass = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(
            colors: [Asset.colorPrimaryDark, Asset.colorPrimary]),
        title: Text(
          'Universitas Teknokrat Indonesia',
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Container(
              height: 150,
              width: 150,
              child: Image(
                  image: NetworkImage(
                      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/UNIVERSITASTEKNOKRAT.png/480px-UNIVERSITASTEKNOKRAT.png")),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Silahkan Login',
              style: TextStyle(
                  fontSize: 25,
                  color: Asset.colorSecoundary,
                  fontWeight: FontWeight.bold),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextFormField(
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      controller: _controllerUserName,
                      style: TextStyle(
                        color: Asset.colorSecoundary,
                      ),
                      decoration: InputDecoration(
                          hintText: 'username',
                          hintStyle: TextStyle(
                            color: Asset.colorSecoundary,
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Asset.colorSecoundary,
                          )),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      obscureText: true,
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      controller: _controllerPass,
                      style: TextStyle(
                        color: Asset.colorSecoundary,
                      ),
                      decoration: InputDecoration(
                          hintText: 'password',
                          hintStyle: TextStyle(
                            color: Asset.colorSecoundary,
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorSecoundary,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.lock,
                            color: Asset.colorSecoundary,
                          )),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      child: GestureDetector(
                        onTap: () {
                          // Aksi yang ingin dilakukan saat teks ditekan
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ResetPassword()),
                          );
                          // Misalnya, buka halaman reset password
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPasswordPage()));
                        },
                        child: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: 'Lupa Password? ',
                            style: TextStyle(color: Colors.black),
                            children: [
                              TextSpan(
                                text: 'Klik Disini',
                                style: TextStyle(
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(colors: [
                          Asset.colorPrimaryDark,
                          Asset.colorPrimary
                        ]),
                      ),
                      width: double.infinity,
                      child: InkWell(
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            EventDb.login(
                                _controllerUserName.text, _controllerPass.text);
                            _controllerUserName.clear();
                            _controllerPass.clear();
                          }
                        },
                        borderRadius: BorderRadius.circular(10),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 30,
                            vertical: 12,
                          ),
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Developed by Kelompok 6',
                          style: TextStyle(
                              color: Color.fromRGBO(173, 173, 173, 1)),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
