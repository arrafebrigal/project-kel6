class Api {
  static const _host = "http://192.168.100.19/api_project_kel6";

  static String _user = "$_host/user";
  static String _databarang = "$_host/barang";
  static String _datapengajuan = "$_host/pengajuan";
  static String _datapengembalian = "$_host/pengembalian";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  static String addBarang = "$_databarang/add_barang.php";
  static String deleteBarang = "$_databarang/delete_barang.php";
  static String getBarang = "$_databarang/get_barang.php";
  static String updateBarang = "$_databarang/update_barang.php";

  static String addPengajuan = "$_datapengajuan/add_pengajuan.php";
  static String deletePengajuan = "$_datapengajuan/delete_pengajuan.php";
  static String getPengajuan = "$_datapengajuan/get_pengajuan.php";
  static String updatePengajuan = "$_datapengajuan/update_pengajuan.php";

  static String addPengembalian = "$_datapengembalian/add_pengembalian.php";
  static String deletePengembalian =
      "$_datapengembalian/delete_pengembalian.php";
  static String getPengembalian = "$_datapengembalian/get_pengembalian.php";
  static String updatePengembalian =
      "$_datapengembalian/update_pengembalian.php";
}
