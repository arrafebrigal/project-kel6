import 'dart:ui';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 81, 0, 97);
  static Color colorPrimary = Color.fromARGB(223, 193, 0, 129);
  static Color colorSecoundary = Color.fromARGB(255, 197, 0, 53);
  static Color colorAccent = Color.fromARGB(255, 251, 25, 70);
}
