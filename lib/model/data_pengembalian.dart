class DataPengembalian {
  String? kode_pengembalian;
  String? kode_pengajuan;
  String? tanggal_kembali;

  DataPengembalian({
    this.kode_pengembalian,
    this.kode_pengajuan,
    this.tanggal_kembali,
  });

  factory DataPengembalian.fromJson(Map<String, dynamic> json) =>
      DataPengembalian(
        kode_pengembalian: json['kode_pengembalian'],
        kode_pengajuan: json['kode_pengajuan'],
        tanggal_kembali: json['tanggal_kembali'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengembalian': this.kode_pengembalian,
        'kode_pengajuan': this.kode_pengajuan,
        'tanggal_kembali': this.tanggal_kembali,
      };
}
