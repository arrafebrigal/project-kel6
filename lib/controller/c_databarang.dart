import 'package:get/get.dart';
import 'package:project_kel6/model/data_barang.dart';

class CDataBarang extends GetxController {
  Rx<DataBarang> _databarang = DataBarang().obs;

  DataBarang get user => _databarang.value;

  void setUser(DataBarang datadata_barang) =>
      _databarang.value = datadata_barang;
}
