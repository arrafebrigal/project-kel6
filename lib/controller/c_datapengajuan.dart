import 'package:get/get.dart';
import 'package:project_kel6/model/data_pengajuan.dart';

class CDataPengajuan extends GetxController {
  Rx<DataPengajuan> _datapengajuan = DataPengajuan().obs;

  DataPengajuan get user => _datapengajuan.value;

  void setUser(DataPengajuan dataPengajuan) =>
      _datapengajuan.value = dataPengajuan;
}
