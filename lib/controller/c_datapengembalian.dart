import 'package:get/get.dart';
import 'package:project_kel6/model/data_pengembalian.dart';

class CDataPengembalian extends GetxController {
  Rx<DataPengembalian> _datapengembalian = DataPengembalian().obs;

  DataPengembalian get user => _datapengembalian.value;

  void setUser(DataPengembalian dataPengembalian) =>
      _datapengembalian.value = dataPengembalian;
}
